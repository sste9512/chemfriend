## ChemFriend
An old android project that needed serious refurbishing
made for basic chemistry reference


## An  incomplete model of the Periodic Table of Elements(zoomable with pinch)
![Alt Text](chem_friend_detail_periodictable.gif)
## An example of the Element Detail Page
![Alt Text](chem_friend_detail.gif)


## License
MIT


