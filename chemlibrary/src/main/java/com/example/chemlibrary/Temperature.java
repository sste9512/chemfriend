package com.periodictableelements.periodictableelements.androidchemistrylibrary;

public class Temperature {


    public static final String CELSIUS = "C";
    public static final String FAHRENHEIT = "F";
    public static final String KELVIN = "K";

    public double temperature = 0.0;
    public String tempMetric = "";


    private Temperature(double temperature, String tempMetric) {
        this.temperature = temperature;
        this.tempMetric = tempMetric;


    }

    public double getKelvin()
    {
        switch (tempMetric)
        {
            case CELSIUS:
                return 0;
            case FAHRENHEIT:
                return 1;
            default:
                return temperature;
        }
    }

    public double getCelsius() {
        switch (tempMetric)
        {
            case FAHRENHEIT:
                return 0;
            case KELVIN:
                return 1;
            default:
                return temperature;
        }
    }

    public double getFahrenheit() {
        switch (tempMetric)
        {
            case CELSIUS:
                return (temperature * 1.8) + 32;
            case KELVIN:
                return 1;
            default:
                return temperature;
        }
    }


}
