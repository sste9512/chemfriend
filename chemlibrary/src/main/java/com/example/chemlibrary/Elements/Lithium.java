package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Lithium { 



public String name = "Lithium";

public String symbol = "Li";

public int atomicNumber = 3;

public double atomicWeight = 6.941;

public double density = 0.534;

public double meltingPoint = 553.69;

public double boilingPoint = 1118.15;

public double atomicRadius = 155;

public double covalentRadius = 163;

public double ionicRadius = 0;

public double specificVolume = 13.1;

public double specificHeat = 3.489;

public double heatFusion = 2.89;

public double heatEvap = 148;

public double thermalConduct = 84.8;

public double paulingElectro = 0.98;

public double firstIonEnerg = 519.9;

public String oxidationState = "1";

public String electronConfig = "[He]2s¹";

public String lattice = "BCC";

public double latticeConst = 3.49;
}
