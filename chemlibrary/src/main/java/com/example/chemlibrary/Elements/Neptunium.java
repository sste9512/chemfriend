package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Neptunium { 



public String name = "Neptunium";

public String symbol = "Np";

public int atomicNumber = 93;

public double atomicWeight = 237.048;

public double density = 20.25;

public double meltingPoint = 913;

public double boilingPoint = 4175;

public double atomicRadius = 130;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 21.1;

public double specificHeat = 0;

public double heatFusion = -9.6;

public double heatEvap = 336;

public double thermalConduct = 6.3;

public double paulingElectro = 1.36;

public double firstIonEnerg = 0;

public String oxidationState = "6 5 4 3";

public String electronConfig = "[Rn]5f6d¹7s²";

public String lattice = "ORC";

public double latticeConst = 4.72;
}
