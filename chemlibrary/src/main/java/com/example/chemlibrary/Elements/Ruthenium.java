package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Ruthenium { 



public String name = "Ruthenium";

public String symbol = "Ru";

public int atomicNumber = 44;

public double atomicWeight = 101.07;

public double density = 12.41;

public double meltingPoint = 2583;

public double boilingPoint = 4173;

public double atomicRadius = 134;

public double covalentRadius = 125;

public double ionicRadius = 0;

public double specificVolume = 8.3;

public double specificHeat = 0.238;

public double heatFusion = 25.5;

public double heatEvap = 0;

public double thermalConduct = 117;

public double paulingElectro = 2.2;

public double firstIonEnerg = 710.3;

public String oxidationState = "8 6 4 3 2 0 -2";

public String electronConfig = "[Kr]4d5s¹";

public String lattice = "HEX";

public double latticeConst = 2.7;
}
