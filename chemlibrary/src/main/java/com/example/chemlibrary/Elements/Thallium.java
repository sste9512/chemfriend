package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Thallium { 



public String name = "Thallium";

public String symbol = "Tl";

public int atomicNumber = 81;

public double atomicWeight = 204.3833;

public double density = 11.85;

public double meltingPoint = 576.6;

public double boilingPoint = 1730;

public double atomicRadius = 171;

public double covalentRadius = 148;

public double ionicRadius = 0;

public double specificVolume = 17.2;

public double specificHeat = 0.128;

public double heatFusion = 4.31;

public double heatEvap = 162.4;

public double thermalConduct = 46.1;

public double paulingElectro = 1.62;

public double firstIonEnerg = 588.9;

public String oxidationState = "3 1";

public String electronConfig = "[Xe]4f¹5d¹6s²6p¹";

public String lattice = "HEX";

public double latticeConst = 3.46;
}
