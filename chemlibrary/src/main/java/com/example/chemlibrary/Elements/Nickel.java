package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Nickel { 



public String name = "Nickel";

public String symbol = "Ni";

public int atomicNumber = 28;

public double atomicWeight = 58.6934;

public double density = 8.902;

public double meltingPoint = 1726;

public double boilingPoint = 3005;

public double atomicRadius = 124;

public double covalentRadius = 115;

public double ionicRadius = 0;

public double specificVolume = 6.6;

public double specificHeat = 0.443;

public double heatFusion = 17.61;

public double heatEvap = 378.6;

public double thermalConduct = 90.9;

public double paulingElectro = 1.91;

public double firstIonEnerg = 736.2;

public String oxidationState = "3 2 0";

public String electronConfig = "[Ar]3d4s²";

public String lattice = "FCC";

public double latticeConst = 3.52;
}
