package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Silver { 



public String name = "Silver";

public String symbol = "Ag";

public int atomicNumber = 47;

public double atomicWeight = 107.8682;

public double density = 10.5;

public double meltingPoint = 1235.1;

public double boilingPoint = 2485;

public double atomicRadius = 144;

public double covalentRadius = 134;

public double ionicRadius = 0;

public double specificVolume = 10.3;

public double specificHeat = 0.237;

public double heatFusion = 11.95;

public double heatEvap = 254.1;

public double thermalConduct = 429;

public double paulingElectro = 1.93;

public double firstIonEnerg = 730.5;

public String oxidationState = "2 1";

public String electronConfig = "[Kr]4d5s¹";

public String lattice = "FCC";

public double latticeConst = 4.09;
}
