package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Calcium { 



public String name = "Calcium";

public String symbol = "Ca";

public int atomicNumber = 20;

public double atomicWeight = 40.078;

public double density = 1.55;

public double meltingPoint = 1112;

public double boilingPoint = 1757;

public double atomicRadius = 197;

public double covalentRadius = 174;

public double ionicRadius = 0;

public double specificVolume = 29.9;

public double specificHeat = 0.653;

public double heatFusion = 9.2;

public double heatEvap = 153.6;

public double thermalConduct = 201;

public double paulingElectro = 1;

public double firstIonEnerg = 589.4;

public String oxidationState = "2";

public String electronConfig = "[Ar]4s²";

public String lattice = "FCC";

public double latticeConst = 5.58;
}
