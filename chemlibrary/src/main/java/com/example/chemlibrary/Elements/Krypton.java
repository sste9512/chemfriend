package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Krypton { 



public String name = "Krypton";

public String symbol = "Kr";

public int atomicNumber = 36;

public double atomicWeight = 83.8;

public double density = 2.155;

public String densityDef = "2.155 (@ -153degC)";

public double meltingPoint = 116.6;

public double boilingPoint = 120.85;

public double atomicRadius = 0;

public double covalentRadius = 112;

public double ionicRadius = 0;

public double specificVolume = 32.2;

public double specificHeat = 0.247;

public double heatFusion = 0;

public double heatEvap = 9.05;

public double thermalConduct = 0.0095;

public double paulingElectro = 0;

public double firstIonEnerg = 1350;

public String oxidationState = "2";

public String electronConfig = "[Ar]3d¹4s²4p";

public String lattice = "FCC";

public double latticeConst = 5.72;
}
