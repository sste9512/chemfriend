package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Tantalum { 



public String name = "Tantalum";

public String symbol = "Ta";

public int atomicNumber = 73;

public double atomicWeight = 180.9479;

public double density = 16.654;

public double meltingPoint = 3269;

public double boilingPoint = 5698;

public double atomicRadius = 149;

public double covalentRadius = 134;

public double ionicRadius = 0;

public double specificVolume = 10.9;

public double specificHeat = 0.14;

public double heatFusion = 24.7;

public double heatEvap = 758;

public double thermalConduct = 57.5;

public double paulingElectro = 1.5;

public double firstIonEnerg = 760.1;

public String oxidationState = "5";

public String electronConfig = "[Xe]4f¹5d³6s²";

public String lattice = "BCC";

public double latticeConst = 3.31;
}
