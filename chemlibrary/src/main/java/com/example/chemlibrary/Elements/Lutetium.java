package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Lutetium { 



public String name = "Lutetium";

public String symbol = "Lu";

public int atomicNumber = 71;

public double atomicWeight = 174.967;

public double density = 9.8404;

public double meltingPoint = 1936;

public double boilingPoint = 3668;

public double atomicRadius = 175;

public double covalentRadius = 156;

public double ionicRadius = 0;

public double specificVolume = 17.8;

public double specificHeat = 0.155;

public double heatFusion = 0;

public double heatEvap = 414;

public double thermalConduct = 16.4;

public double paulingElectro = 1.27;

public double firstIonEnerg = 513;

public String oxidationState = "3";

public String electronConfig = "[Xe]4f¹5d¹6s²";

public String lattice = "HEX";

public double latticeConst = 3.51;
}
