package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Scandium { 



public String name = "Scandium";

public String symbol = "Sc";

public int atomicNumber = 21;

public double atomicWeight = 44.95591;

public double density = 2.99;

public double meltingPoint = 1814;

public double boilingPoint = 3104;

public double atomicRadius = 162;

public double covalentRadius = 144;

public double ionicRadius = 0;

public double specificVolume = 15;

public double specificHeat = 0.556;

public double heatFusion = 15.8;

public double heatEvap = 332.7;

public double thermalConduct = 15.8;

public double paulingElectro = 1.36;

public double firstIonEnerg = 630.8;

public String oxidationState = "3";

public String electronConfig = "[Ar]3d¹4s²";

public String lattice = "HEX";

public double latticeConst = 3.31;
}
