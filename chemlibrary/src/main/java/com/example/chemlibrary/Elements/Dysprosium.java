package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Dysprosium { 



public String name = "Dysprosium";

public String symbol = "Dy";

public int atomicNumber = 66;

public double atomicWeight = 162.5;

public double density = 8.55;

public double meltingPoint = 1685;

public double boilingPoint = 2835;

public double atomicRadius = 180;

public double covalentRadius = 159;

public double ionicRadius = 0;

public double specificVolume = 19;

public double specificHeat = 0.173;

public double heatFusion = 0;

public double heatEvap = 291;

public double thermalConduct = 10.7;

public double paulingElectro = 0;

public double firstIonEnerg = 567;

public String oxidationState = "3";

public String electronConfig = "[Xe]4f5d6s²";

public String lattice = "HEX";

public double latticeConst = 3.59;
}
