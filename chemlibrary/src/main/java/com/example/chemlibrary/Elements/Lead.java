package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Lead { 



public String name = "Lead";

public String symbol = "Pb";

public int atomicNumber = 82;

public double atomicWeight = 207.2;

public double density = 11.35;

public double meltingPoint = 600.65;

public double boilingPoint = 2013;

public double atomicRadius = 175;

public double covalentRadius = 147;

public double ionicRadius = 0;

public double specificVolume = 18.3;

public double specificHeat = 0.159;

public double heatFusion = 4.77;

public double heatEvap = 177.8;

public double thermalConduct = 35.3;

public double paulingElectro = 1.8;

public double firstIonEnerg = 715.2;

public String oxidationState = "4 2";

public String electronConfig = "[Xe]4f¹5d¹6s²6p²";

public String lattice = "FCC";

public double latticeConst = 4.95;
}
