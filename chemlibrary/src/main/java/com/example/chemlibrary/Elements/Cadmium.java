package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Cadmium { 



public String name = "Cadmium";

public String symbol = "Cd";

public int atomicNumber = 48;

public double atomicWeight = 112.411;

public double density = 8.65;

public double meltingPoint = 594.1;

public double boilingPoint = 1038;

public double atomicRadius = 154;

public double covalentRadius = 148;

public double ionicRadius = 0;

public double specificVolume = 13.1;

public double specificHeat = 0.232;

public double heatFusion = 6.11;

public double heatEvap = 59.1;

public double thermalConduct = 96.9;

public double paulingElectro = 1.69;

public double firstIonEnerg = 867.2;

public String oxidationState = "2";

public String electronConfig = "[Kr]4d5s²";

public String lattice = "HEX";

public double latticeConst = 2.98;
}
