package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Strontium { 



public String name = "Strontium";

public String symbol = "Sr";

public int atomicNumber = 38;

public double atomicWeight = 87.62;

public double density = 2.54;

public double meltingPoint = 1042;

public double boilingPoint = 1657;

public double atomicRadius = 215;

public double covalentRadius = 191;

public double ionicRadius = 0;

public double specificVolume = 33.7;

public double specificHeat = 0.301;

public double heatFusion = 9.2;

public double heatEvap = 144;

public double thermalConduct = 35.4;

public double paulingElectro = 0.95;

public double firstIonEnerg = 549;

public String oxidationState = "2";

public String electronConfig = "[Kr]5s²";

public String lattice = "FCC";

public double latticeConst = 6.08;
}
