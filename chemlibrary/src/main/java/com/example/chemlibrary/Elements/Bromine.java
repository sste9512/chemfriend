package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Bromine { 



public String name = "Bromine";

public String symbol = "Br";

public int atomicNumber = 35;

public double atomicWeight = 79.904;

public double density = 3.12;

public double meltingPoint = 265.9;

public double boilingPoint = 331.9;

public double atomicRadius = 0;

public double covalentRadius = 114;

public double ionicRadius = 0;

public double specificVolume = 23.5;

public double specificHeat = 0.473;

public double heatFusion = 10.57;

public double heatEvap = 29.56;

public double thermalConduct = 0.005;

public double paulingElectro = 2.96;

public double firstIonEnerg = 1142;

public String oxidationState = "7 5 3 1 -1";

public String electronConfig = "[Ar]3d¹4s²4p";

public String lattice = "ORC";

public double latticeConst = 6.67;
}
