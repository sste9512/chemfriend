package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Thorium { 



public String name = "Thorium";

public String symbol = "Th";

public int atomicNumber = 90;

public double atomicWeight = 232.0381;

public double density = 11.78;

public double meltingPoint = 2028;

public double boilingPoint = 5060;

public double atomicRadius = 180;

public double covalentRadius = 165;

public double ionicRadius = 0;

public double specificVolume = 19.8;

public double specificHeat = 0.113;

public double heatFusion = 16.11;

public double heatEvap = 513.7;

public double thermalConduct = 54;

public double paulingElectro = 1.3;

public double firstIonEnerg = 670.4;

public String oxidationState = "4";

public String electronConfig = "[Rn]5f6d¹7s²";

public String lattice = "FCC";

public double latticeConst = 5.08;
}
