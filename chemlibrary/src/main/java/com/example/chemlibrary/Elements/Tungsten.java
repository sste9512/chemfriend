package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Tungsten { 



public String name = "Tungsten";

public String symbol = "W";

public int atomicNumber = 74;

public double atomicWeight = 183.84;

public double density = 19.3;

public double meltingPoint = 3680;

public double boilingPoint = 5930;

public double atomicRadius = 141;

public double covalentRadius = 130;

public double ionicRadius = 0;

public double specificVolume = 9.53;

public double specificHeat = 0.133;

public double heatFusion = -35;

public double heatEvap = 824;

public double thermalConduct = 173;

public double paulingElectro = 1.7;

public double firstIonEnerg = 769.7;

public String oxidationState = "6 5 4 3 2 0";

public String electronConfig = "[Xe]4f¹5d6s²";

public String lattice = "BCC";

public double latticeConst = 3.16;
}
