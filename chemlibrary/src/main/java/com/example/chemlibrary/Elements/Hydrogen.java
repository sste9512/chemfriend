package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Hydrogen { 



public String name = "Hydrogen";

public String symbol = "H";

public int atomicNumber = 1;

public double atomicWeight = 1.00794;

public double density = 0.0708;

public String densityDef = "0.0708 (@ -253degC)";

public double meltingPoint = 14.01;

public double boilingPoint = 20.28;

public double atomicRadius = 79;

public double covalentRadius = 32;

public double ionicRadius = 0;

public double specificVolume = 14.1;

public double specificHeat = 14.267 ;

public double heatFusion = 0.117 ;

public double heatEvap = 0.904;

public double thermalConduct = 0.1815;

public double paulingElectro = 2.2;

public double firstIonEnerg = 1311.3;

public String oxidationState = "1 -1";

public String electronConfig = "1s¹";

public String lattice = "HEX";

public double latticeConst = 3.75;
}
