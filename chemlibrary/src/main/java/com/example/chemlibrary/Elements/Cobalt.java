package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Cobalt { 



public String name = "Cobalt";

public String symbol = "Co";

public int atomicNumber = 27;

public double atomicWeight = 58.9332;

public double density = 8.9;

public double meltingPoint = 1768;

public double boilingPoint = 3143;

public double atomicRadius = 125;

public double covalentRadius = 116;

public double ionicRadius = 0;

public double specificVolume = 6.7;

public double specificHeat = 0.456;

public double heatFusion = 15.48;

public double heatEvap = 389.1;

public double thermalConduct = 100;

public double paulingElectro = 1.88;

public double firstIonEnerg = 758.1;

public String oxidationState = "3 2 0 -1";

public String electronConfig = "[Ar]3d4s²";

public String lattice = "HEX";

public double latticeConst = 2.51;
}
