package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Copper { 



public String name = "Copper";

public String symbol = "Cu";

public int atomicNumber = 29;

public double atomicWeight = 63.546;

public double density = 8.96;

public double meltingPoint = 1356.6;

public double boilingPoint = 2840;

public double atomicRadius = 128;

public double covalentRadius = 117;

public double ionicRadius = 0;

public double specificVolume = 7.1;

public double specificHeat = 0.385;

public double heatFusion = 13.01;

public double heatEvap = 304.6;

public double thermalConduct = 401;

public double paulingElectro = 1.9;

public double firstIonEnerg = 745;

public String oxidationState = "2 1";

public String electronConfig = "[Ar]3d¹4s¹";

public String lattice = "FCC";

public double latticeConst = 3.61;
}
