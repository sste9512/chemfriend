package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Beryllium { 



public String name = "Beryllium";

public String symbol = "Be";

public int atomicNumber = 4;

public double atomicWeight = 9.01218;

public double density = 1.848;

public double meltingPoint = 1551;

public double boilingPoint = 3243;

public double atomicRadius = 112;

public double covalentRadius = 90;

public double ionicRadius = 0;

public double specificVolume = 5;

public double specificHeat = 1.824;

public double heatFusion = 12.21;

public double heatEvap = 309;

public double thermalConduct = 201;

public double paulingElectro = 1.57;

public double firstIonEnerg = 898.8;

public String oxidationState = "2";

public String electronConfig = "[He]2s²";

public String lattice = "HEX";

public double latticeConst = 2.29;
}
