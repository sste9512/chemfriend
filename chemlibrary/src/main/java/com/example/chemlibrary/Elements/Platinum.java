package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Platinum { 



public String name = "Platinum";

public String symbol = "Pt";

public int atomicNumber = 78;

public double atomicWeight = 195.08;

public double density = 21.45;

public double meltingPoint = 2045;

public double boilingPoint = 4100;

public double atomicRadius = 139;

public double covalentRadius = 130;

public double ionicRadius = 0;

public double specificVolume = 9.1;

public double specificHeat = 0.133;

public double heatFusion = 21.76;

public double heatEvap = ~470;

public double thermalConduct = 71.6;

public double paulingElectro = 2.28;

public double firstIonEnerg = 868.1;

public String oxidationState = "4 2 0";

public String electronConfig = "[Xe]4f¹5d6s²";

public String lattice = "FCC";

public double latticeConst = 3.92;
}
