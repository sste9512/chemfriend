package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Francium { 



public String name = "Francium";

public String symbol = "Fr";

public int atomicNumber = 87;

public double atomicWeight = 223.0197;

public double density = 0;

public double meltingPoint = 300;

public double boilingPoint = 950;

public double atomicRadius = 0;

public double covalentRadius = 0;

public double ionicRadius = 0;

public double specificVolume = 0;

public double specificHeat = 0;

public double heatFusion = 15;

public double heatEvap = 0;

public double thermalConduct = 0;

public double paulingElectro = 0.7;

public double firstIonEnerg = ~375;

public String oxidationState = "2";

public String electronConfig = "[Rn]7s¹";

public String lattice = "BCC";

public double latticeConst = 0;
}
