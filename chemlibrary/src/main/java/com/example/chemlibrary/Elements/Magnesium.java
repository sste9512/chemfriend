package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Magnesium { 



public String name = "Magnesium";

public String symbol = "Mg";

public int atomicNumber = 12;

public double atomicWeight = 24.305;

public double density = 1.738;

public double meltingPoint = 922;

public double boilingPoint = 1363;

public double atomicRadius = 160;

public double covalentRadius = 136;

public double ionicRadius = 0;

public double specificVolume = 14;

public double specificHeat = 1.025;

public double heatFusion = 9.2;

public double heatEvap = 131.8;

public double thermalConduct = 156;

public double paulingElectro = 1.31;

public double firstIonEnerg = 737.3;

public String oxidationState = "2";

public String electronConfig = "[Ne]3s²";

public String lattice = "HEX";

public double latticeConst = 3.21;
}
