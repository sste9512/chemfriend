package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Tellurium { 



public String name = "Tellurium";

public String symbol = "Te";

public int atomicNumber = 52;

public double atomicWeight = 127.6;

public double density = 6.24;

public double meltingPoint = 722.7;

public double boilingPoint = 1263;

public double atomicRadius = 160;

public double covalentRadius = 136;

public double ionicRadius = 0;

public double specificVolume = 20.5;

public double specificHeat = 0.201;

public double heatFusion = 17.91;

public double heatEvap = 49.8;

public double thermalConduct = 14.3;

public double paulingElectro = 2.1;

public double firstIonEnerg = 869;

public String oxidationState = "6 4 2";

public String electronConfig = "[Kr]4d5s²5p";

public String lattice = "HEX";

public double latticeConst = 4.45;
}
