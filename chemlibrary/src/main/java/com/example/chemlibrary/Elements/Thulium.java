package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Thulium { 



public String name = "Thulium";

public String symbol = "Tm";

public int atomicNumber = 69;

public double atomicWeight = 168.93421;

public double density = 9.321;

public double meltingPoint = 1818;

public double boilingPoint = 2220;

public double atomicRadius = 177;

public double covalentRadius = 156;

public double ionicRadius = 0;

public double specificVolume = 18.1;

public double specificHeat = 0.16;

public double heatFusion = 0;

public double heatEvap = 232;

public double thermalConduct = 16.9;

public double paulingElectro = 1.25;

public double firstIonEnerg = 589;

public String oxidationState = "3 2";

public String electronConfig = "[Xe]4f¹³5d6s²";

public String lattice = "HEX";

public double latticeConst = 3.54;
}
