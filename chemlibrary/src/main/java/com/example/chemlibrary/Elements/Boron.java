package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Boron { 



public String name = "Boron";

public String symbol = "B";

public int atomicNumber = 5;

public double atomicWeight = 10.811;

public double density = 2.34;

public double meltingPoint = 2573;

public double boilingPoint = 3931;

public double atomicRadius = 98;

public double covalentRadius = 82;

public double ionicRadius = 0;

public double specificVolume = 4.6;

public double specificHeat = 1.025;

public double heatFusion = 23.6;

public double heatEvap = 504.5;

public double thermalConduct = 27.4;

public double paulingElectro = 2.04;

public double firstIonEnerg = 800.2;

public String oxidationState = "3";

public String electronConfig = "[He]2s²2p¹";

public String lattice = "TET";

public double latticeConst = 8.73;
}
