package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Selenium { 



public String name = "Selenium";

public String symbol = "Se";

public int atomicNumber = 34;

public double atomicWeight = 78.96;

public double density = 4.79;

public double meltingPoint = 490;

public double boilingPoint = 958.1;

public double atomicRadius = 140;

public double covalentRadius = 116;

public double ionicRadius = 0;

public double specificVolume = 16.5;

public double specificHeat = 0.321;

public double heatFusion = 5.23;

public double heatEvap = 59.7;

public double thermalConduct = 0.52;

public double paulingElectro = 2.55;

public double firstIonEnerg = 940.4;

public String oxidationState = "6 4 -2";

public String electronConfig = "[Ar]3d¹4s²4p";

public String lattice = "HEX";

public double latticeConst = 4.36;
}
