package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Praseodymium { 



public String name = "Praseodymium";

public String symbol = "Pr";

public int atomicNumber = 59;

public double atomicWeight = 140.90765;

public double density = 6.773;

public double meltingPoint = 1204;

public double boilingPoint = 3785;

public double atomicRadius = 182;

public double covalentRadius = 165;

public double ionicRadius = 0;

public double specificVolume = 20.8;

public double specificHeat = 0.192;

public double heatFusion = 11.3;

public double heatEvap = 331;

public double thermalConduct = 12.5;

public double paulingElectro = 1.13;

public double firstIonEnerg = 526.6;

public String oxidationState = "4 3";

public String electronConfig = "[Xe]4f³5d6s²";

public String lattice = "HEX";

public double latticeConst = 3.67;
}
