package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Tin { 



public String name = "Tin";

public String symbol = "Sn";

public int atomicNumber = 50;

public double atomicWeight = 118.71;

public double density = 7.31;

public double meltingPoint = 505.1;

public double boilingPoint = 2543;

public double atomicRadius = 162;

public double covalentRadius = 141;

public double ionicRadius = 0;

public double specificVolume = 16.3;

public double specificHeat = 0.222;

public double heatFusion = 7.07;

public double heatEvap = 296;

public double thermalConduct = 66.8;

public double paulingElectro = 1.96;

public double firstIonEnerg = 708.2;

public String oxidationState = "4 2";

public String electronConfig = "[Kr]4d5s²5p²";

public String lattice = "TET";

public double latticeConst = 5.82;
}
