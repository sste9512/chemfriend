package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Indium { 



public String name = "Indium";

public String symbol = "In";

public int atomicNumber = 49;

public double atomicWeight = 114.818;

public double density = 7.31;

public double meltingPoint = 429.32;

public double boilingPoint = 2353;

public double atomicRadius = 166;

public double covalentRadius = 144;

public double ionicRadius = 0;

public double specificVolume = 15.7;

public double specificHeat = 0.234;

public double heatFusion = 3.24;

public double heatEvap = 225.1;

public double thermalConduct = 81.8;

public double paulingElectro = 1.78;

public double firstIonEnerg = 558;

public String oxidationState = "3";

public String electronConfig = "[Kr]4d5s²5p¹";

public String lattice = "TET";

public double latticeConst = 4.59;
}
