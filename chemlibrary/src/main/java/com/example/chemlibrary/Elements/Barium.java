package com.example.chemlibrary.Elements;

public class Barium {


    public String name = "Barium";

    public String symbol = "Ba";

    public int atomicNumber = 56;

    public double atomicWeight = 137.327;

    public double density = 3.5;

    public double meltingPoint = 1002;

    public double boilingPoint = 1910;

    public double atomicRadius = 222;

    public double covalentRadius = 198;

    public double ionicRadius = 0;

    public double specificVolume = 39;

    public double specificHeat = 0.192;

    public double heatFusion = 7.66;

    public double heatEvap = 142;

    public double thermalConduct = 18.4;

    public double paulingElectro = 0.89;

    public double firstIonEnerg = 502.5;

    public String oxidationState = "2";

    public String electronConfig = "[Xe]6s²";

    public String lattice = "BCC";

    public double latticeConst = 5.02;
}
