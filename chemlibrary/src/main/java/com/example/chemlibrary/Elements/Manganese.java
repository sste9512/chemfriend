package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Manganese { 



public String name = "Manganese";

public String symbol = "Mn";

public int atomicNumber = 25;

public double atomicWeight = 54.93805;

public double density = 7.21;

public double meltingPoint = 1517;

public double boilingPoint = 2235;

public double atomicRadius = 135;

public double covalentRadius = 117;

public double ionicRadius = 0;

public double specificVolume = 7.39;

public double specificHeat = 0.477;

public double heatFusion = -13.4;

public double heatEvap = 221;

public double thermalConduct = 7.8;

public double paulingElectro = 1.55;

public double firstIonEnerg = 716.8;

public String oxidationState = "7 6 4 3 2 0 -1";

public String electronConfig = "[Ar]3d4s²";

public String lattice = "CUB";

public double latticeConst = 8.89;
}
