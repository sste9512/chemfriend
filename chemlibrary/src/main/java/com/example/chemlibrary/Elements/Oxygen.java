package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Oxygen { 



public String name = "Oxygen";

public String symbol = "O";

public int atomicNumber = 8;

public double atomicWeight = 15.9994;

public double density = 1.149;

public String densityDef = "1.149 (@ -183degC)";

public double meltingPoint = 54.8;

public double boilingPoint = 90.19;

public double atomicRadius = 0;

public double covalentRadius = 73;

public double ionicRadius = 0;

public double specificVolume = 14;

public double specificHeat = 0.916;

public double heatFusion = 0;

public double heatEvap = 0;

public double thermalConduct = 0.027;

public double paulingElectro = 3.44;

public double firstIonEnerg = 1313.1;

public String oxidationState = "-2 -1";

public String electronConfig = "[He]2s²2p";

public String lattice = "CUB";

public double latticeConst = 6.83;
}
