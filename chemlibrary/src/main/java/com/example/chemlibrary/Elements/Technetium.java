package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Technetium { 



public String name = "Technetium";

public String symbol = "Tc";

public int atomicNumber = 43;

public double atomicWeight = 97.9072;

public double density = 11.5;

public double meltingPoint = 2445;

public double boilingPoint = 5150;

public double atomicRadius = 136;

public double covalentRadius = 127;

public double ionicRadius = 0;

public double specificVolume = 8.5;

public double specificHeat = 0.243;

public double heatFusion = 23.8;

public double heatEvap = 585;

public double thermalConduct = 50.6;

public double paulingElectro = 1.9;

public double firstIonEnerg = 702.2;

public String oxidationState = "7";

public String electronConfig = "[Kr]4d5s¹";

public String lattice = "HEX";

public double latticeConst = 2.74;
}
