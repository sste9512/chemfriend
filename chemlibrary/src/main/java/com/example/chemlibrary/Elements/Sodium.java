package com.periodictableelements.periodictableelements.androidchemistrylibrary.Elements;
public class Sodium { 



public String name = "Sodium";

public String symbol = "Na";

public int atomicNumber = 11;

public double atomicWeight = 22.989768;

public double density = 0.971;

public double meltingPoint = 370.96;

public double boilingPoint = 1156.1;

public double atomicRadius = 190;

public double covalentRadius = 154;

public double ionicRadius = 0;

public double specificVolume = 23.7;

public double specificHeat = 1.222;

public double heatFusion = 2.64;

public double heatEvap = 97.9;

public double thermalConduct = 142;

public double paulingElectro = 0.93;

public double firstIonEnerg = 495.6;

public String oxidationState = "1";

public String electronConfig = "[Ne]3s¹";

public String lattice = "BCC";

public double latticeConst = 4.23;
}
