package com.chemfriend.steve.chemfriend;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.chemfriend.steve.chemfriend.Adapter.AtomicGridAdapter;
import com.chemfriend.steve.chemfriend.ExpandingCollectionView.Expanding_Main;
import com.chemfriend.steve.chemfriend.Persistence.DbContext;
import com.chemfriend.steve.chemfriend.Views.SearchDialog;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, AtomicGridAdapter.ItemClickListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.rvNumbers)
    RecyclerView rvNumbers;
    @BindView(R.id.bottomTable)
    RecyclerView bottomTable;
    @BindView(R.id.atom_search_input)
    EditText atomSearchInput;

    public AtomicGridAdapter adapter;
    public AtomicGridAdapter adapter2;
    public DbContext context;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);
        context = new DbContext();
        initTables();
        atomSearchInput.setOnClickListener(v -> {
                SearchDialog dialog = new SearchDialog(MainActivity.this, R.style.FullScreenDialogStyle);
                dialog.show();
        });

    }


    private void initTables() {

        int[] data = {1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2,
                3, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 5, 6, 7, 8, 9, 10,
                11, 12, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 13, 14, 15, 16, 17, 18,
                19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36,
                37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54,
                55, 56, -1, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86,
                87, 88, -1, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118};


        int[] data2 = {57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71,
                89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103};


        int numberOfColumns = 18;
        rvNumbers.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        //adapter = new AtomicGridAdapter(this, data);
        adapter = new AtomicGridAdapter(this, data, context.chemList);
        //adapter.setClickListener(this);
        rvNumbers.canScrollVertically(0);
        rvNumbers.setAdapter(adapter);


        int numberOfColumns2 = 15;
        bottomTable.setLayoutManager(new GridLayoutManager(this, numberOfColumns2));
        adapter2 = new AtomicGridAdapter(this, data2, context.chemList);
        //adapter2 = new AtomicGridAdapter(this, data2);
        //adapter2.setClickListener(this);
        bottomTable.canScrollVertically(0);
        bottomTable.setNestedScrollingEnabled(false);
        bottomTable.setAdapter(adapter2);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {
            this.startActivity(new Intent(this, Expanding_Main.class));

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }


    @OnClick(R.id.fab)
    public void onViewClicked() {

    }

    @Override
    public void onItemClick(View view, int position) {

    }
}
