package com.chemfriend.steve.chemfriend.Persistence;

import android.os.Build;
import android.support.annotation.RequiresApi;

import com.chemfriend.steve.chemfriend.Models.Atom;
import com.example.chemlibrary.PeriodicElements;


import java.util.ArrayList;
import java.util.function.Function;
import java.util.function.Predicate;

public class DbContext implements IDbContext {

    DbConfig config;

    public ArrayList<Atom> chemList;

    public DbContext() {
        chemList = new ArrayList<>();
        GatherAll();
      /*  if(!IsDBInitialised()){
            InitDB();
        }
        else{
            QueryAll();
        }*/
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
        public ArrayList<Atom> where(Predicate<String> predicate){
            ArrayList<Atom> query = new ArrayList<>();
            for (Atom atom: chemList) {
                if (predicate.test(atom.name)){
                    query.add(atom);
                }
            }
            return query;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public Atom firstOrDefault(Predicate<String> predicate){
        for (Atom atom: chemList) {
             if(predicate.test(atom.name)){
                 return atom;
             }
        }
        return chemList.get(0);
    }

    private void GatherAll() {
        for (int i = 0; i <= 116; i++) {
            chemList.add(new Atom(PeriodicElements.findElementByNumber(i)));
        }
    }

    @Override
    public Boolean IsDBInitialised() {
        config = DbConfig.findById(DbConfig.class, (long) 0);
        return config.IsInitialised;
    }

    @Override
    public void InitDB() {
        GatherAll();
        SaveAll();
    }

    @Override
    public void QueryAll() {
        for (int i = 0; i <= 117; i++) {
            Atom atom = Atom.findById(Atom.class, (long) i);
            chemList.add(atom);
        }
    }

    @Override
    public void SaveAll() {
        for (int i = 0; i <= 117; i++) {
            chemList.get(i).save();
        }
    }


}
