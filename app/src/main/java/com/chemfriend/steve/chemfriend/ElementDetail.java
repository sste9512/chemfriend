package com.chemfriend.steve.chemfriend;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ElementDetail extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;
    @BindView(R.id.app_bar)
    AppBarLayout appBar;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.background_header)
    ImageView backgroundHeader;


    @BindView(R.id.atomicNumber)
    TextView atomicNumber;
    @BindView(R.id.atomicSymbol)
    TextView atomicSymbol;
    @BindView(R.id.atomicGroup)
    TextView atomicGroup;
    @BindView(R.id.elec_config)
    TextView elecConfig;
    @BindView(R.id.melting_point)
    TextView meltingPoint;
    @BindView(R.id.boiling_point)
    TextView boilingPoint;
    @BindView(R.id.heat_of_fusion)
    TextView heatOfFusion;
    @BindView(R.id.heat_of_vaporization)
    TextView heatOfVaporization;
    @BindView(R.id.molar_heat_capacity)
    TextView molarHeatCapacity;
    @BindView(R.id.density)
    TextView density;
    @BindView(R.id.liquid_mp)
    TextView liquidMp;
    @BindView(R.id.oxidation_states)
    TextView oxidationStates;
    @BindView(R.id.electronegativity)
    TextView electronegativity;
    @BindView(R.id.ionization_energies)
    TextView ionizationEnergies;
    @BindView(R.id.atomic_radius)
    TextView atomicRadius;
    @BindView(R.id.covalent_radius)
    TextView covalentRadius;
    @BindView(R.id.van_der_waals_radius)
    TextView vanDerWaalsRadius;


    String chemName = "";

    public static void passElementName(String chemName, Context context) {
        Intent intent = new Intent(context, ElementDetail.class);
        intent.putExtra("Element", chemName);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.element_detail);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            chemName = extras.getString("Element");
            //USE CHEM LIBRARY ELEMENT FINDER STATIC METHOD


            //#1 FIND ELEMENT OBJECT VIA FINDER METHODS

            //#2 INIT UI BASED ON ELEMENT


        } else {
            //#1 CREATE NO ELEMENT SCREEN
        }

        ButterKnife.bind(this);
        Picasso.with(ElementDetail.this)
                .load(R.drawable.aluminum)
                .fit()
                .into(backgroundHeader);
        toolbar.setTitle("Aluminum");

    }


    @OnClick(R.id.fab)
    public void onViewClicked() {

    }

}
