package com.chemfriend.steve.chemfriend.Utils;

import android.os.AsyncTask;

public class Task extends AsyncTask {


    private IBackgroundAction action;
    private ICompleted completed;

    public Task(IBackgroundAction action, ICompleted completed) {
        this.action = action;
        this.completed = completed;
    }

    public Task(IBackgroundAction action) {
        this.action = action;
    }

    @Override
    protected Object doInBackground(Object[] objects) {
        action.Action();
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(Object o) {
        if (completed != null) {
            completed.OnCompleted();
        }
        super.onPostExecute(o);
    }


    @Override
    protected void onCancelled() {
        super.onCancelled();
    }

    public static void Start(IBackgroundAction action, ICompleted completed) {
        Task task = new Task(action, completed);
        task.execute(null, null, null);
    }

    public static void Start(IBackgroundAction action) {
        Task task = new Task(action);
        task.execute(null, null, null);
    }

}
