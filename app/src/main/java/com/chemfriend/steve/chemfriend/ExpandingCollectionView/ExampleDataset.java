package com.chemfriend.steve.chemfriend.ExpandingCollectionView;


import com.chemfriend.steve.chemfriend.R;
import com.ramotion.expandingcollection.ECCardData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class ExampleDataset {

    private List<ECCardData> dataset;

    public ExampleDataset() {
        dataset = new ArrayList<>(5);


        CardData item10 = new CardData();
        item10.setMainBackgroundResource(R.drawable.white_vertex);
        item10.setHeadBackgroundResource(R.drawable.blue_vertex);
        item10.setHeadTitle("Noble Gases");
        item10.setPersonMessage("Cur adelphis studere?");
        item10.setPersonName("Wallace Sutton");
        item10.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item10.setListItems(prepareCommentsArray());
        dataset.add(item10);


        CardData item9 = new CardData();
        item9.setMainBackgroundResource(R.drawable.blue_vertex);
        item9.setHeadBackgroundResource(R.drawable.blue_vertex);
        item9.setHeadTitle("Halogens");
        item9.setPersonMessage("Cur adelphis studere?");
        item9.setPersonName("Wallace Sutton");
        item9.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item9.setListItems(prepareCommentsArray());
        dataset.add(item9);



        CardData item8 = new CardData();
        item8.setMainBackgroundResource(R.drawable.yellow_vertex);
        item8.setHeadBackgroundResource(R.drawable.yellow_vertex);
        item8.setHeadTitle("Nonmetals");
        item8.setPersonMessage("Cur adelphis studere?");
        item8.setPersonName("Wallace Sutton");
        item8.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item8.setListItems(prepareCommentsArray());
        dataset.add(item8);



        CardData item7 = new CardData();
        item7.setMainBackgroundResource(R.drawable.white_vertex);
        item7.setHeadBackgroundResource(R.drawable.blue_vertex);
        item7.setHeadTitle("Metalloid");
        item7.setPersonMessage("Cur adelphis studere?");
        item7.setPersonName("Wallace Sutton");
        item7.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item7.setListItems(prepareCommentsArray());
        dataset.add(item7);




        CardData item6 = new CardData();
        item6.setMainBackgroundResource(R.drawable.white_vertex2);
        item6.setHeadBackgroundResource(R.drawable.white_vertex2);
        item6.setHeadTitle("Post-Transition Metals");
        item6.setPersonMessage("C");
        item6.setPersonName("Wallace Sutton");
        item6.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item6.setListItems(prepareCommentsArray());
        dataset.add(item6);




        CardData item5 = new CardData();
        item5.setMainBackgroundResource(R.drawable.white_vertex);
        item5.setHeadBackgroundResource(R.drawable.galaxy);
        item5.setHeadTitle("Alkali Metals");
        item5.setPersonMessage("Group 1");
        item5.setPersonName("Lithium through Francium");
        item5.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item5.setListItems(prepareCommentsArray());
        dataset.add(item5);

        CardData item4 = new CardData();
        item4.setMainBackgroundResource(R.drawable.blue_vertex);
        item4.setHeadBackgroundResource(R.drawable.blue_vertex);
        item4.setHeadTitle("Alkaline Earth Metals");
        item4.setPersonMessage("Group 2");
        item4.setPersonName("Beryllium through Radium");
        item4.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item4.setListItems(prepareCommentsArray());
        dataset.add(item4);

        CardData item3 = new CardData();
        item3.setMainBackgroundResource(R.drawable.light_blue_vertex);
        item3.setHeadBackgroundResource(R.drawable.light_blue_vertex);
        item3.setHeadTitle("Lathanides");
        item3.setPersonMessage("The f-elements");
        item3.setPersonName("Lanthanum through Lutetium");
        item3.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item3.setListItems(prepareCommentsArray());
        dataset.add(item3);

        CardData item2 = new CardData();
        item2.setMainBackgroundResource(R.drawable.blue_vertex);
        item2.setHeadBackgroundResource(R.drawable.blue_vertex);
        item2.setHeadTitle("Actinides");
        item2.setPersonName("The F-Elements");
        item2.setPersonMessage("Actinium through Lawrencium");
        item2.setListItems(prepareCommentsArray());
        item2.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        dataset.add(item2);

        CardData item1 = new CardData();
        item1.setMainBackgroundResource(R.drawable.blue_vertex);
        item1.setHeadBackgroundResource(R.drawable.blue_vertex);
        item1.setHeadTitle("Transition Metals");
        item1.setPersonMessage("Group 3 through Group 12, including the Actinides");
        item1.setPersonName("Elements that have a partially filled d subshell");
        item1.setPersonPictureResource(R.drawable.hexagon_frame_icon);
        item1.setListItems(prepareCommentsArray());
        dataset.add(item1);

    }

    public List<ECCardData> getDataset() {
        Collections.shuffle(dataset);
        return dataset;
    }




    private List<Comment> prepareCommentsArray() {
        Random random = new Random();
        List<Comment> comments = new ArrayList<>();
        comments.addAll(Arrays.asList(
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life"),
                new Comment(R.drawable.hexagon_frame_icon, "ElementName", "Element Description", "Half Life")
                ));
        Collections.shuffle(comments);
        return comments.subList(0, 6 + random.nextInt(5));
    }
}
