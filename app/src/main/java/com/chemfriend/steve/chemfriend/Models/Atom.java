package com.chemfriend.steve.chemfriend.Models;

import com.orm.SugarRecord;

import java.lang.reflect.Field;


public class Atom extends SugarRecord<Atom> {

    public String name;

    public String symbol;

    public int atomicNumber;

    public double atomicWeight;

    public double density;

    public double meltingPoint;

    public double boilingPoint;

    public double atomicRadius;

    public double covalentRadius;

    public double ionicRadius;

    public double specificVolume;

    public double specificHeat;

    public double heatFusion;

    public double heatEvap;

    public double thermalConduct;

    public double paulingElectro;

    public double firstIonEnerg;

    public String oxidationState;

    public String electronConfig;

    public String lattice;

    public double latticeConst;


    public Atom(Object element) {
        try {
            MapToElement(element);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private void MapToElement(Object element) throws IllegalAccessException {
        Object el = element;
        Field[] fields = el.getClass().getDeclaredFields();

        for (Field field : el.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if(field.getName().equals("name")){
                name = String.valueOf(field.get(el));
            }
            if(field.getName().equals("symbol")){
                symbol = String.valueOf(field.get(el));
            }
            if(field.getName().equals("atomicNumber")){
                atomicNumber = (int) field.get(el);
            }
            if(field.getName().equals("atomicWeight")){
                atomicWeight = (double) field.get(el);
            }
            if(field.getName().equals("density")){
                density = (double) field.get(el);
            }
            if(field.getName().equals("meltingPoint")){
                meltingPoint = (double) field.get(el);
            }
            if(field.getName().equals("boilingPoint")){
                boilingPoint = (double) field.get(el);
            }
            if(field.getName().equals("atomicRadius")){
                atomicRadius = (double) field.get(el);
            }
            if(field.getName().equals("covalentRadius")){
                covalentRadius = (double) field.get(el);
            }
            if(field.getName().equals("ionicRadius")){
                ionicRadius = (double) field.get(el);
            }
            if(field.getName().equals("specificVolume")){
                specificVolume = (double) field.get(el);
            }
            if(field.getName().equals("specificHeat")){
                specificHeat = (double) field.get(el);
            }
            if(field.getName().equals("heatFusion")){
                heatFusion = (double) field.get(el);
            }
            if(field.getName().equals("heatEvap")){
                heatEvap = (double) field.get(el);
            }
            if(field.getName().equals("thermalConduct")){
                thermalConduct = (double) field.get(el);
            }
            if(field.getName().equals("paulingElectro")){
                paulingElectro = (double) field.get(el);
            }
            if(field.getName().equals("firstIonEnerg")){
                firstIonEnerg = (double) field.get(el);
            }
            if(field.getName().equals("oxidationState")){
                oxidationState = String.valueOf(field.get(el));
            }
            if(field.getName().equals("electronConfig")){
                electronConfig = String.valueOf(field.get(el));
            }
            if(field.getName().equals("lattice")){
                lattice = String.valueOf(field.get(el));
            }
            if(field.getName().equals("latticeConst")){
                latticeConst = (double) field.get(el);
            }

        }
    }

}
