package com.chemfriend.steve.chemfriend.ExpandingCollectionView;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;

import com.appolica.flubber.Flubber;
import com.chemfriend.steve.chemfriend.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ChemicalDetailPage extends Activity {

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chem_detail_page);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        Flubber.with()
                .animation(Flubber.AnimationPreset.SLIDE_DOWN)
                .interpolator(Flubber.Curve.BZR_EASE_IN)
                .duration(500)
                .autoStart(true)
                .createFor(fab);
    }
}


