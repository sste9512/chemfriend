package com.chemfriend.steve.chemfriend;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;

import com.chemfriend.steve.chemfriend.Persistence.DbContext;

public class ChemFriend extends Application {

    public static Typeface myCustomFont;

    public static DbContext ctx;

    public ChemFriend() {
        super();
     }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        myCustomFont = Typeface.createFromAsset(this.getAssets(), "fonts/RobotoCondensed-Regular.ttf");
        ctx = new DbContext();
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }



    @Override
    public void onLowMemory() {
        super.onLowMemory();

    }



    @Override
    public void onTerminate() {
        super.onTerminate();
    }






}
