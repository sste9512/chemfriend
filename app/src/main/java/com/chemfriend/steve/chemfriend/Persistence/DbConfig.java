package com.chemfriend.steve.chemfriend.Persistence;

import com.orm.SugarRecord;

public class DbConfig extends SugarRecord<DbConfig> {
      boolean IsInitialised = false;

    public boolean isInitialised() {
        return IsInitialised;
    }

    public void setInitialised(boolean initialised) {
        IsInitialised = initialised;
    }
}
