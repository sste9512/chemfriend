package com.chemfriend.steve.chemfriend.ExpandingCollectionView;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.chemfriend.steve.chemfriend.DesignHelpers.CircleTransform;
import com.chemfriend.steve.chemfriend.R;
import com.chemfriend.steve.chemfriend.ElementDetail;
import com.ramotion.expandingcollection.ECBackgroundSwitcherView;
import com.ramotion.expandingcollection.ECCardData;
import com.ramotion.expandingcollection.ECPagerView;
import com.ramotion.expandingcollection.ECPagerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;


@SuppressLint("SetTextI18n")
public class Expanding_Main extends AppCompatActivity
{

    @BindView(R.id.ec_bg_switcher_element)
    ECBackgroundSwitcherView ecBgSwitcherElement;
    @BindView(R.id.items_count_view)
    ItemsCountView itemsCountView;
    @BindView(R.id.ec_pager_element)
    ECPagerView ecPagerElement;
    private ECPagerView ecPagerView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.expanding_main);
        ButterKnife.bind(this);

        // Create adapter for pager
        ECPagerViewAdapter adapter = new ECPagerViewAdapter(this, new ExampleDataset().getDataset()) {




            @Override
            public void instantiateCard(LayoutInflater inflaterService, final ViewGroup head, ListView list, final ECCardData data) {
                final CardData cardData = (CardData) data;


                // Create adapter for list inside a card and set adapter to card content
                CommentArrayAdapter commentArrayAdapter = new CommentArrayAdapter(getApplicationContext(), cardData.getListItems());
                list.setAdapter(commentArrayAdapter);
                list.setDivider(getResources().getDrawable(R.drawable.list_divider));
                list.setDividerHeight((int) pxFromDp(getApplicationContext(), 0.5f));
                list.setSelector(R.color.transparent);
                list.setBackgroundColor(Color.BLACK);
                list.setCacheColorHint(Color.TRANSPARENT);
                list.setOnItemClickListener((parent, view, position, id) -> {
                    // ElementDetail.passElementName();
                    startActivity(new Intent(Expanding_Main.this, ElementDetail.class));

                });

                // Add gradient to root header view
                View gradient = new View(getApplicationContext());
                gradient.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, AbsListView.LayoutParams.MATCH_PARENT));
                gradient.setBackgroundDrawable(getResources().getDrawable(R.drawable.card_head_gradient));
                head.addView(gradient);

                // Inflate main header layout and attach it to header root view
                inflaterService.inflate(R.layout.simple_head, head);

                // Set header data from data object
                final TextView title = (TextView) head.findViewById(R.id.title);
                title.setText(cardData.getHeadTitle());
                ImageView avatar = (ImageView) head.findViewById(R.id.avatar);
                Picasso.with(Expanding_Main.this)
                        .load(R.drawable.hexagon_frame_icon)
                        .fit()
                        .into(avatar);
                TextView name = (TextView) head.findViewById(R.id.name);
                name.setText(cardData.getPersonName() + ":");
                TextView message = (TextView) head.findViewById(R.id.message);
                message.setText(cardData.getPersonMessage());



                // Add onclick listener to card header for toggle card state
                head.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        ecPagerView.toggle();
                    }
                });
            }
        };

        ecPagerView = (ECPagerView) findViewById(R.id.ec_pager_element);
        ecPagerView.setPagerViewAdapter(adapter);
        ecPagerView.setBackgroundSwitcherView((ECBackgroundSwitcherView) findViewById(R.id.ec_bg_switcher_element));


        ecPagerView.setOnCardSelectedListener((newPosition, oldPosition, totalElements) -> itemsCountView.update(newPosition, oldPosition, totalElements));
    }


    @Override
    public void onBackPressed() {
        if (!ecPagerView.collapse())
            super.onBackPressed();
    }


    public static float dpFromPx(final Context context, final float px) {
        return px / context.getResources().getDisplayMetrics().density;
    }


    public static float pxFromDp(final Context context, final float dp) {
        return dp * context.getResources().getDisplayMetrics().density;
    }

}
