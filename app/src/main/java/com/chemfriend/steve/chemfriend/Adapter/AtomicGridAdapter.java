package com.chemfriend.steve.chemfriend.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chemfriend.steve.chemfriend.Models.Atom;
import com.chemfriend.steve.chemfriend.R;
import com.example.chemlibrary.PeriodicElements;

import java.util.ArrayList;


/*
   View adapter for setting the specific colors and formation of the periodic table recycler view
 */
/*

  Fix the -1 indexes from showing, or replace with unique view

 */
public class AtomicGridAdapter extends RecyclerView.Adapter<AtomicGridAdapter.ViewHolder> {


    private static final int POST_TRANSITION_METAL = 0;
    private static final int TRANSITION_METAL = 1;
    private static final int ALKALINE_EARTH_METAL = 2;
    private static final int ALKALI_METAL = 3;
    private static final int LANTALOID = 4;
    private static final int ACTINOID = 5;

    private ArrayList<Atom> atoms;
    private int[] mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;


    // data is passed into the constructor
    public AtomicGridAdapter(Context context, int[] data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
    }

    public AtomicGridAdapter(Context context, int[] data, ArrayList<Atom> atoms) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.atoms = atoms;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.table_cell, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (mData[position] != 0 && mData[position] != -1 && mData[position] < 118) {
            Atom atom = new Atom(PeriodicElements.findElementByNumber(mData[position]));
            holder.atomicSymbol.setText(atom.symbol);
            holder.atomicNumber.setText(String.valueOf(atom.atomicNumber));
            holder.atomicName.setText(atom.name);
            if (mData[position] == 0) {
                holder.cardView.setAlpha(0.0f);
            }
            if (mData[position] == 1) {
                setTextColors(holder, "#81CFE0");
            } else if (isTransitionMetal(mData[position])) {
                setTextColors(holder, "#86E2D5");
            } else if (isAlkali(mData[position])) {
                setTextColors(holder, "#E9D460");
            } else if (isHalogen(mData[position])) {
                setTextColors(holder, "#81CFE0");
            } else if (isNobleGas(mData[position])) {
                setTextColors(holder, "#913D88");
            } else if (isAlkaliMetal(mData[position])) {
                setTextColors(holder, "#FFFDA6");
            } else if (mData[position] >= 57 && mData[position] <= 71) //ACTINOIDS
            {
                setTextColors(holder, "#E74C3C");
            } else if (mData[position] >= 89 && mData[position] <= 103)//POST TRANSITION METALS
            {
                setTextColors(holder, "#BE90D4");
            } else {

            }

        }
        else{
            holder.atomicName.setVisibility(View.INVISIBLE);
            holder.atomicNumber.setVisibility(View.INVISIBLE);
            holder.atomicSymbol.setVisibility(View.INVISIBLE);
        }
    }

    private boolean isTransitionMetal(int position) {
        switch (position) {
            case 21:
                return true;
            case 22:
                return true;
            case 23:
                return true;
            case 24:
                return true;
            case 25:
                return true;
            case 26:
                return true;
            case 27:
                return true;
            case 28:
                return true;
            case 29:
                return true;
            case 30:
                return true;
            case 39:
                return true;
            case 40:
                return true;
            case 41:
                return true;
            case 42:
                return true;
            case 43:
                return true;
            case 44:
                return true;
            case 45:
                return true;
            case 46:
                return true;
            case 47:
                return true;
            case 48:
                return true;
            case 49:
                return true;
            case 72:
                return true;
            case 73:
                return true;
            case 74:
                return true;
            case 75:
                return true;
            case 76:
                return true;
            case 77:
                return true;
            case 78:
                return true;
            case 79:
                return true;
            case 80:
                return true;
            case 104:
                return true;
            case 105:
                return true;
            case 106:
                return true;
            case 107:
                return true;
            case 108:
                return true;
            case 109:
                return true;
            case 110:
                return true;
            case 111:
                return true;
            case 112:
                return true;
            default:
                return false;
        }
    }

    private boolean isAlkali(int position) {
        switch (position) {
            case 5:
                return true;
            case 14:
                return true;
            case 32:
                return true;
            case 33:
                return true;
            case 51:
                return true;
            case 52:
                return true;
            case 84:
                return true;
            default:
                return false;
        }
    }

    private boolean isNonMetal(int position) {
        switch (position) {
            case 6:
                return true;
            case 7:
                return true;
            case 8:
                return true;
            case 15:
                return true;
            case 16:
                return true;
            case 34:
                return true;
            default:
                return false;
        }
    }

    private boolean isHalogen(int position) {
        switch (position) {
            case 9:
                return true;
            case 17:
                return true;
            case 35:
                return true;
            case 53:
                return true;
            case 85:
                return true;
            case 117:
                return true;
            default:
                return false;
        }
    }

    private boolean isNobleGas(int position) {
        switch (position) {
            case 2:
                return true;
            case 10:
                return true;
            case 18:
                return true;
            case 36:
                return true;
            case 54:
                return true;
            case 86:
                return true;
            case 118:
                return true;
            default:
                return false;
        }
    }

    private boolean isAlkaliMetal(int position) {
        switch (position) {
            case 3:
                return true;
            case 11:
                return true;
            case 19:
                return true;
            case 37:
                return true;
            case 55:
                return true;
            case 87:
                return true;
            default:
                return false;
        }
    }


    private void setTextColors(ViewHolder holder, String color) {
        holder.atomicSymbol.setTextColor(Color.parseColor(color));
        holder.atomicName.setTextColor(Color.parseColor(color));
        holder.atomicNumber.setTextColor(Color.parseColor(color));
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.length;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView atomicSymbol;
        TextView atomicNumber;
        TextView atomicName;
        LinearLayout cell_layout;
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            atomicSymbol = (TextView) itemView.findViewById(R.id.atomicSymbol);
            atomicName = (TextView) itemView.findViewById(R.id.atomicName);
            atomicNumber = (TextView) itemView.findViewById(R.id.atomicNumber);
            cell_layout = (LinearLayout) itemView.findViewById(R.id.cell_layout);
            cardView = (CardView) itemView.findViewById(R.id.card_layout);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }


    // convenience method for getting data at click position
    int getItem(int id) {
        return mData[id];
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}

