package com.chemfriend.steve.chemfriend.Persistence;

public interface IDbContext {

    Boolean IsDBInitialised();
    void InitDB();
    void QueryAll();
    void SaveAll();
}
