package com.chemfriend.steve.chemfriend.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appolica.flubber.Flubber;
import com.chemfriend.steve.chemfriend.Models.Atom;
import com.chemfriend.steve.chemfriend.R;
import com.example.chemlibrary.PeriodicElements;

import java.util.ArrayList;

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultsAdapter.ViewHolder> {

    private ArrayList<Atom> atoms;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    public SearchResultsAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        atoms = new ArrayList<>();
    }

    public SearchResultsAdapter(Context context, ArrayList<Atom> atoms) {
        this.mInflater = LayoutInflater.from(context);
        this.atoms = atoms;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.table_cell, parent, false);
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Atom atom = atoms.get(position);
         holder.atomicSymbol.setText(atom.symbol);
         holder.atomicNumber.setText(String.valueOf(atom.atomicNumber));
         holder.atomicName.setText(atom.name);


    }

    @Override
    public void onViewAttachedToWindow(@NonNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        Flubber.with()
                .animation(Flubber.AnimationPreset.FADE_IN) // Slide up animation
                .duration(300)                              // Last for 1000 milliseconds(1 second)
                .createFor(holder.itemView)                 // Apply it to the view
                .start();
    }

    @Override
    public void onViewDetachedFromWindow(@NonNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        Flubber.with()
                .animation(Flubber.AnimationPreset.FADE_IN_UP) // Slide up animation
                .duration(300)                                 // Last for 1000 milliseconds(1 second)
                .createFor(holder.itemView)                    // Apply it to the view
                .start();
    }


    private void setTextColors(ViewHolder holder, String color) {
        holder.atomicSymbol.setTextColor(Color.parseColor(color));
        holder.atomicName.setTextColor(Color.parseColor(color));
        holder.atomicNumber.setTextColor(Color.parseColor(color));
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return atoms.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView atomicSymbol;
        TextView atomicNumber;
        TextView atomicName;
        LinearLayout cell_layout;
        CardView cardView;

        ViewHolder(View itemView) {
            super(itemView);
            atomicSymbol = (TextView) itemView.findViewById(R.id.atomicSymbol);
            atomicName = (TextView) itemView.findViewById(R.id.atomicName);
            atomicNumber = (TextView) itemView.findViewById(R.id.atomicNumber);
            cell_layout = (LinearLayout) itemView.findViewById(R.id.cell_layout);
            cardView = (CardView) itemView.findViewById(R.id.card_layout);
            itemView.setOnClickListener(this);
        }





        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());

        }
    }


    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    // convenience method for getting data at click position
    int getItem(int id) {
        return 2;
    }

    //  allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
