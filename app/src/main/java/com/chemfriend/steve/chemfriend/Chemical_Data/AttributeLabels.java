package com.chemfriend.steve.chemfriend.Chemical_Data;



public class AttributeLabels {

    public static final String[] attribute_labels = {
            "Name",
            "Symbol",
            "Atomic Number",
            "Atomic Weight",
            "Density",
            "Melting Point",
            "Boiling Point",
            "Atomic Radius",
            "Covalent Radius",
            "Ionic Radius",
            "Specific Volume",
            "Specific Heat",
            "Heat Fusion",
            "Heat Evaporation",
            "Thermal Conductivity",
            "Pauling Elec Neg",
            "First Ion Energy",
            "Oxidation States",
            "Electronic Config",
            "Lattice",
            "Lattice Constant"
    };
}
