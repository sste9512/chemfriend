package com.chemfriend.steve.chemfriend.Views;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.PeriodicSync;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.chemfriend.steve.chemfriend.Adapter.SearchResultsAdapter;
import com.chemfriend.steve.chemfriend.Models.Atom;
import com.chemfriend.steve.chemfriend.R;
import com.example.chemlibrary.PeriodicElements;

import java.util.ArrayList;
import java.util.Objects;

public class SearchDialog extends Dialog implements TextWatcher{

    EditText searchInput;
    RecyclerView searchResults;
    SearchResultsAdapter adapter;
    ArrayList<Atom> atoms;

    public SearchDialog(Context context) {
        super(context);
        setContentView(R.layout.dialog_search);
        getWindow().setFormat(PixelFormat.RGB_565);
        Objects.requireNonNull(getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        searchInput = (EditText)findViewById(R.id.search_input);
        searchResults = (RecyclerView)findViewById(R.id.search_result);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        searchInput.addTextChangedListener(this);
}

    public SearchDialog(Context context, int themeResId) {
        super(context, themeResId);
        setContentView(R.layout.dialog_search);
        atoms = new ArrayList<>();
        getWindow().setFormat(PixelFormat.RGB_565);
        Objects.requireNonNull(getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        searchInput = (EditText)findViewById(R.id.search_input);
        searchResults = (RecyclerView)findViewById(R.id.search_result);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.BLACK));
        searchInput.addTextChangedListener(this);

        adapter = new SearchResultsAdapter(context,atoms);
        searchResults.setLayoutManager(new GridLayoutManager(context, 5));

        //adapter.setClickListener(this);
        //adapter.canScrollVertically(0);
        searchResults.setAdapter(adapter);

        //Add Item

        //notify adapter

    }

    protected SearchDialog(Context context, boolean cancelable,DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);

    }

    @Override
    public void show() {
        Window window = getWindow();
        if (window != null) {
            window.setGravity(Gravity.BOTTOM);
            //window.setWindowAnimations(R.style.CustomDialog);
        }
        super.show();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
         atoms.add(new Atom(PeriodicElements.findElementByNumber(2)));
         adapter.notifyDataSetChanged();
    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}
